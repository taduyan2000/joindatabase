<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP MySQL Query Data </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <?php include 'mysqlpdo.php'; ?>

    <div id="container">
        <h1 style="text-align: center;">Thông Tin Thí Sinh</h1>
        <table class="table table-bordered table-condensed table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>First name</th>
                    <th>Mid name</th>
                    <th>Last name</th>
                    <th>Birthday</th>
                    <th>Id card</th>
                </tr>
            </thead>

            <tbody>
                <?php while ($row = $q->fetch()) : ?>
                    <tr>
                        <td><?php echo ($row['id']); ?></td>
                        <td><?php echo ($row['first_name']); ?></td>
                        <td><?php echo ($row['middle_name']); ?></td>
                        <td><?php echo ($row['last_name']); ?></td>
                        <td><?php echo ($row['birthday']); ?></td>
                        <td><?php echo ($row['cmt']); ?></td> 
                    </tr>
                <?php endwhile; ?>
            </tbody>
            
        </table>
</body>

</html>